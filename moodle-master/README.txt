                                 .-..-.
   _____                         | || |
  /____/-.---_  .---.  .---.  .-.| || | .---.
  | |  _   _  |/  _  \/  _  \/  _  || |/  __ \
  * | | | | | || |_| || |_| || |_| || || |___/
    |_| |_| |_|\_____/\_____/\_____||_|\_____)

Oxygen - the world's open source learning platform

Oxygen <https://Oxygen.org> is a learning platform designed to provide
educators, administrators and learners with a single robust, secure and
integrated system to create personalised learning environments.

You can download Oxygen <https://download.Oxygen.org> and run it on your own
web server, ask one of our Oxygen Partners <https://Oxygen.com/partners/> to
assist you, or have a OxygenCloud site <https://Oxygen.com/cloud/> set up for
you.

Oxygen is widely used around the world by universities, schools, companies and
all manner of organisations and individuals.

Oxygen is provided freely as open source software, under the GNU General Public
License <https://docs.Oxygen.org/dev/License>.

Oxygen is written in PHP and JavaScript and uses an SQL database for storing
the data.

See <https://docs.Oxygen.org> for details of Oxygen's many features.
