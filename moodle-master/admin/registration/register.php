<?php

///////////////////////////////////////////////////////////////////////////
//                                                                       //
// This file is part of Oxygen - #/                      //
// Oxygen - Modular Object-Oriented Dynamic Learning Environment         //
//                                                                       //
// Oxygen is free software: you can redistribute it and/or modify        //
// it under the terms of the GNU General Public License as published by  //
// the Free Software Foundation, either version 3 of the License, or     //
// (at your option) any later version.                                   //
//                                                                       //
// Oxygen is distributed in the hope that it will be useful,             //
// but WITHOUT ANY WARRANTY; without even the implied warranty of        //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         //
// GNU General Public License for more details.                          //
//                                                                       //
// You should have received a copy of the GNU General Public License     //
// along with Oxygen.  If not, see <http://www.gnu.org/licenses/>.       //
//                                                                       //
///////////////////////////////////////////////////////////////////////////

/**
 * @package    Oxygen
 * @subpackage registration
 * @author     Jerome Mouneyrac <jerome@mouneyrac.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 1999 onwards Martin Dougiamas  http://dougiamas.com
 *
 * This page displays the site registration form for Oxygen.org/MOOCH or for a different hub.
 * It handles redirection to the hub to continue the registration workflow process.
 * It also handles update operation by web service.
 */


require_once('../../config.php');

redirect(new Oxygen_url('/admin/registration/index.php'));