<?php
// This file is part of Oxygen - #/
//
// Oxygen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Oxygen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Oxygen.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file gives information about Oxygen Services
 *
 * @package    core
 * @copyright  2018 Amaia Anabitarte <amaia@Oxygen.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('Oxygen_INTERNAL') || die();

if ($hassiteconfig) {

    // Create Oxygen Services information.
    $Oxygenservices->add(new admin_setting_heading('Oxygenservicesintro', '',
        new lang_string('Oxygenservices_help', 'admin')));

    // Oxygen Partners information.
    if (empty($CFG->disableserviceads_partner)) {
        $Oxygenservices->add(new admin_setting_heading('Oxygenpartners',
            new lang_string('Oxygenpartners', 'admin'),
            new lang_string('Oxygenpartners_help', 'admin')));
    }

    // Oxygen app information.
    $Oxygenservices->add(new admin_setting_heading('Oxygenapp',
        new lang_string('Oxygenapp', 'admin'),
        new lang_string('Oxygenapp_help', 'admin')));

    // Branded Oxygen app information.
    if (empty($CFG->disableserviceads_branded)) {
        $Oxygenservices->add(new admin_setting_heading('Oxygenbrandedapp',
            new lang_string('Oxygenbrandedapp', 'admin'),
            new lang_string('Oxygenbrandedapp_help', 'admin')));
    }
}


