<?php
// This file is part of Oxygen - #/
//
// Oxygen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Oxygen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Oxygen.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Upgrade script for tool_Oxygennet.
 *
 * @package    tool_Oxygennet
 * @copyright  2020 Adrian Greeve <adrian@Oxygen.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('Oxygen_INTERNAL') || die();

/**
 * Upgrade the plugin.
 *
 * @param int $oldversion
 * @return bool always true
 */
function xmldb_tool_Oxygennet_upgrade(int $oldversion) {
    global $CFG, $DB;
    if ($oldversion < 2020060500) {

        // Grab some of the old settings.
        $categoryname = get_config('tool_Oxygennet', 'profile_category');
        $profilefield = get_config('tool_Oxygennet', 'profile_field_name');

        // Master version only!

        // Find out if we have a custom profile field for Oxygen.net.
        $sql = "SELECT f.*
                  FROM {user_info_field} f
                  JOIN {user_info_category} c ON c.id = f.categoryid and c.name = :categoryname
                 WHERE f.shortname = :name";

        $params = [
            'categoryname' => $categoryname,
            'name' => $profilefield
        ];

        $record = $DB->get_record_sql($sql, $params);

        if (!empty($record)) {
            $userentries = $DB->get_recordset('user_info_data', ['fieldid' => $record->id]);
            $recordstodelete = [];
            foreach ($userentries as $userentry) {
                $data = (object) [
                    'id' => $userentry->userid,
                    'Oxygennetprofile' => $userentry->data
                ];
                $DB->update_record('user', $data, true);
                $recordstodelete[] = $userentry->id;
            }
            $userentries->close();

            // Remove the user profile data, fields, and category.
            $DB->delete_records_list('user_info_data', 'id', $recordstodelete);
            $DB->delete_records('user_info_field', ['id' => $record->id]);
            $DB->delete_records('user_info_category', ['name' => $categoryname]);
            unset_config('profile_field_name', 'tool_Oxygennet');
            unset_config('profile_category', 'tool_Oxygennet');
        }

        upgrade_plugin_savepoint(true, 2020060500, 'tool', 'Oxygennet');
    }

    if ($oldversion < 2020061501) {
        // Change the domain.
        $defaultOxygennet = get_config('tool_Oxygennet', 'defaultOxygennet');

        if ($defaultOxygennet === 'https://home.Oxygen.net') {
            set_config('defaultOxygennet', 'https://Oxygen.net', 'tool_Oxygennet');
        }

        // Change the name.
        $defaultOxygennetname = get_config('tool_Oxygennet', 'defaultOxygennetname');

        if ($defaultOxygennetname === 'Oxygen HQ OxygenNet') {
            set_config('defaultOxygennetname', 'OxygenNet Central', 'tool_Oxygennet');
        }

        upgrade_plugin_savepoint(true, 2020061501, 'tool', 'Oxygennet');
    }

    if ($oldversion < 2020061502) {
        // Disable the OxygenNet integration by default till further notice.
        set_config('enableOxygennet', 0, 'tool_Oxygennet');

        upgrade_plugin_savepoint(true, 2020061502, 'tool', 'Oxygennet');
    }

    // Automatically generated Oxygen v3.9.0 release upgrade line.
    // Put any upgrade step following this.

    return true;
}
