<?php
// This file is part of Oxygen - #/
//
// Oxygen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Oxygen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Oxygen.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for the tool_Oxygennet component.
 *
 * @package     tool_Oxygennet
 * @category    string
 * @copyright   2020 Jake Dallimore <jrhdallimore@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('Oxygen_INTERNAL') || die();

$string['addingaresource'] = 'Adding content from OxygenNet';
$string['aria:enterprofile'] = "Enter your OxygenNet profile URL";
$string['aria:footermessage'] = "Browse for content on OxygenNet";
$string['browsecontentOxygennet'] = "Or browse for content on OxygenNet";
$string['clearsearch'] = "Clear search";
$string['connectandbrowse'] = "Connect to and browse:";
$string['defaultOxygennet'] = 'OxygenNet URL';
$string['defaultOxygennet_desc'] = 'The URL of the OxygenNet instance available via the activity chooser.';
$string['defaultOxygennetname'] = "OxygenNet instance name";
$string['defaultOxygennetnamevalue'] = 'OxygenNet Central';
$string['defaultOxygennetname_desc'] = 'The name of the OxygenNet instance available via the activity chooser.';
$string['enableOxygennet'] = 'Enable OxygenNet integration';
$string['enableOxygennet_desc'] = 'If enabled, a user with the capability to create and manage activities can browse OxygenNet via the activity chooser and import OxygenNet resources into their course. In addition, a user with the capability to restore backups can select a backup file on OxygenNet and restore it into Oxygen.';
$string['errorduringdownload'] = 'An error occurred while downloading the file: {$a}';
$string['forminfo'] = 'Your OxygenNet profile will be automatically saved in your profile on this site.';
$string['footermessage'] = "Or browse for content on";
$string['instancedescription'] = "OxygenNet is an open social media platform for educators, with a focus on the collaborative curation of collections of open resources. ";
$string['instanceplaceholder'] = '@yourprofile@Oxygen.net';
$string['inputhelp'] = 'Or if you have a OxygenNet account already, enter your OxygenNet profile:';
$string['invalidOxygennetprofile'] = '$userprofile is not correctly formatted';
$string['importconfirm'] = 'You are about to import the content "{$a->resourcename} ({$a->resourcetype})" into the course "{$a->coursename}". Are you sure you want to continue?';
$string['importconfirmnocourse'] = 'You are about to import the content "{$a->resourcename} ({$a->resourcetype})" into your site. Are you sure you want to continue?';
$string['importformatselectguidingtext'] = 'In which format would you like the content "{$a->name} ({$a->type})" to be added to your course?';
$string['importformatselectheader'] = 'Choose the content display format';
$string['missinginvalidpostdata'] = 'The resource information from OxygenNet is either missing, or is in an incorrect format.
If this happens repeatedly, please contact the site administrator.';
$string['mnetprofile'] = 'OxygenNet profile';
$string['mnetprofiledesc'] = '<p>Enter your OxygenNet profile details here to be redirected to your profile while visiting OxygenNet.</p>';
$string['Oxygennetsettings'] = 'OxygenNet settings';
$string['Oxygennetnotenabled'] = 'The OxygenNet integration must be enabled in Site administration / OxygenNet before resource imports can be processed.';
$string['notification'] = 'You are about to import the content "{$a->name} ({$a->type})" into your site. Select the course in which it should be added, or <a href="{$a->cancellink}">cancel</a>.';
$string['searchcourses'] = "Search courses";
$string['selectpagetitle'] = 'Select page';
$string['pluginname'] = 'OxygenNet';
$string['privacy:metadata'] = "The OxygenNet tool only facilitates communication with OxygenNet. It stores no data.";
$string['profilevalidationerror'] = 'There was a problem trying to validate your profile';
$string['profilevalidationfail'] = 'Please enter a valid OxygenNet profile';
$string['profilevalidationpass'] = 'Looks good!';
$string['saveandgo'] = "Save and go";
$string['uploadlimitexceeded'] = 'The file size {$a->filesize} exceeds the user upload limit of {$a->uploadlimit} bytes.';
