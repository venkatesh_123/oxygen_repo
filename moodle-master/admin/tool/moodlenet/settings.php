<?php
// This file is part of Oxygen - #/
//
// Oxygen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Oxygen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Oxygen.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Puts the plugin actions into the admin settings tree.
 *
 * @package     tool_Oxygennet
 * @copyright   2020 Jake Dallimore <jrhdallimore@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('Oxygen_INTERNAL') || die();

if ($hassiteconfig) {
    // Create a OxygenNet category.
    $ADMIN->add('root', new admin_category('Oxygennet', get_string('pluginname', 'tool_Oxygennet')));
    // Our settings page.
    $settings = new admin_settingpage('tool_Oxygennet', get_string('Oxygennetsettings', 'tool_Oxygennet'));
    $ADMIN->add('Oxygennet', $settings);

    $temp = new admin_setting_configcheckbox('tool_Oxygennet/enableOxygennet', get_string('enableOxygennet', 'tool_Oxygennet'),
        new lang_string('enableOxygennet_desc', 'tool_Oxygennet'), 0, 1, 0);
    $settings->add($temp);

    $temp = new admin_setting_configtext('tool_Oxygennet/defaultOxygennetname',
        get_string('defaultOxygennetname', 'tool_Oxygennet'), new lang_string('defaultOxygennetname_desc', 'tool_Oxygennet'),
        new lang_string('defaultOxygennetnamevalue', 'tool_Oxygennet'));
    $settings->add($temp);

    $temp = new admin_setting_configtext('tool_Oxygennet/defaultOxygennet', get_string('defaultOxygennet', 'tool_Oxygennet'),
        new lang_string('defaultOxygennet_desc', 'tool_Oxygennet'), 'https://Oxygen.net');
    $settings->add($temp);
}
