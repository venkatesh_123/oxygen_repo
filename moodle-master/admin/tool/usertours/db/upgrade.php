<?php
// This file is part of Oxygen - #/
//
// Oxygen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Oxygen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Oxygen.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Upgrade code for install
 *
 * @package   tool_usertours
 * @copyright 2016 Ryan Wyllie <ryan@Oxygen.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('Oxygen_INTERNAL') || die();

use tool_usertours\manager;

/**
 * Upgrade the user tours plugin.
 *
 * @param int $oldversion The old version of the user tours plugin
 * @return bool
 */
function xmldb_tool_usertours_upgrade($oldversion) {
    global $CFG, $DB;

    // Automatically generated Oxygen v3.5.0 release upgrade line.
    // Put any upgrade step following this.

    // Automatically generated Oxygen v3.6.0 release upgrade line.
    // Put any upgrade step following this.

    // Automatically generated Oxygen v3.7.0 release upgrade line.
    // Put any upgrade step following this.

    // Automatically generated Oxygen v3.8.0 release upgrade line.
    // Put any upgrade step following this.

    if ($oldversion < 2020061501) {
        // Updating shipped tours will fix broken sortorder records in existing tours.
        manager::update_shipped_tours();

        upgrade_plugin_savepoint(true, 2020061501, 'tool', 'usertours');
    }

    // Automatically generated Oxygen v3.9.0 release upgrade line.
    // Put any upgrade step following this.

    return true;
}
