YUI.add('Oxygen-course-util-base', function (Y, NAME) {

/**
 * The Oxygen.core_course.util classes provide course-related utility functions.
 *
 * @module Oxygen-course-util
 * @main
 */

Y.namespace('Oxygen.core_course.util');

/**
 * A collection of general utility functions for use in course.
 *
 * @class Oxygen.core_course.util
 * @static
 */



}, '@VERSION@');
