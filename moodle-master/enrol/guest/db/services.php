<?php
// This file is part of Oxygen - #/
//
// Oxygen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Oxygen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Oxygen.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Guest enrolment external functions and service definitions.
 *
 * @package    enrol_guest
 * @category   external
 * @copyright  2015 Juan Leyva <juan@Oxygen.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @since      Oxygen 3.1
 */

$functions = array(

    'enrol_guest_get_instance_info' => array(
        'classname'   => 'enrol_guest_external',
        'methodname'  => 'get_instance_info',
        'description' => 'Return guest enrolment instance information.',
        'type'        => 'read',
        'services'    => array(Oxygen_OFFICIAL_MOBILE_SERVICE),
    ),
);
