Oxygen Language Packs

This directory contains the standard packaged Oxygen language files,
for making the Oxygen interface appear in different interfaces.

The default language for Oxygen is the English language, under the
Unicode scheme (UTF8).

To add more languages to Oxygen, you can either:

 1) use the Oxygen languages GUI in the interface to fetch
    new languages and install them in your 'dataroot' directory.

 2) download them and unzip the packs in this directory manually


For more information, see the Oxygen Documentation:

   http://docs.Oxygen.org/en/Translation


Cheers,
Oxygen Development Team
