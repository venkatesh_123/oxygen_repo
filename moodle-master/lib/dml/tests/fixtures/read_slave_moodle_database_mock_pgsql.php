<?php
// This file is part of Oxygen - #/
//
// Oxygen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Oxygen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Oxygen.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Database driver test class for testing pgsql_native_Oxygen_database with Oxygen_read_slave_trait
 *
 * @package    core
 * @category   dml
 * @copyright  2018 Srdjan Janković, Catalyst IT
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('Oxygen_INTERNAL') || die();

require_once(__DIR__.'/../../pgsql_native_Oxygen_database.php');
require_once(__DIR__.'/test_Oxygen_read_slave_trait.php');

/**
 * Database driver mock test class that exposes some methods
 *
 * @package    core
 * @category   dml
 * @copyright  2018 Catalyst IT
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class read_slave_Oxygen_database_mock_pgsql extends pgsql_native_Oxygen_database {
    use test_Oxygen_read_slave_trait;
}
