<?php
// This file is part of Oxygen - #/
//
// Oxygen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Oxygen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Oxygen.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for Oxygen emoticon plugin.
 *
 * @package   tinymce_Oxygenemoticon
 * @copyright 2012 The Open University
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Insert emoticon';
$string['requireemoticon'] = 'Require emoticon filter';
$string['requireemoticon_desc'] = 'If enabled the emoticon button is visible only when the emoticon filter is enabled in the editor context.';

/* All lang strings used from TinyMCE JavaScript code must be named 'pluginname:stringname', no need to create langs/en_dlg.js */
$string['Oxygenemoticon:desc'] = 'Insert emoticon';
$string['privacy:metadata'] = 'The Oxygen Emoticon TinyMCE plugin does not store any personal data.';
