/**
 * @author Dongsheng Cai <dongsheng@Oxygen.com>
 */

(function() {
    var each = tinymce.each;

    tinymce.PluginManager.requireLangPack('Oxygenmedia');

    tinymce.create('tinymce.plugins.OxygenmediaPlugin', {
        init : function(ed, url) {
            var t = this;

            t.editor = ed;
            t.url = url;

            // Register commands.
            ed.addCommand('mceOxygenMedia', function() {
                ed.windowManager.open({
                    file : url + '/Oxygenmedia.htm',
                    width : 480 + parseInt(ed.getLang('media.delta_width', 0)),
                    height : 480 + parseInt(ed.getLang('media.delta_height', 0)),
                    inline : 1
                }, {
                    plugin_url : url
                });
            });

            // Register buttons.
            ed.addButton('Oxygenmedia', {
                    title : 'Oxygenmedia.desc',
                    image : url + '/img/icon.png',
                    cmd : 'mceOxygenMedia'});

        },

        _parse : function(s) {
            return tinymce.util.JSON.parse('{' + s + '}');
        },

        getInfo : function() {
            return {
                longname : 'Oxygen media',
                author : 'Dongsheng Cai <dongsheng@Oxygen.com>',
                version : "1.0"
            };
        }

    });

    // Register plugin.
    tinymce.PluginManager.add('Oxygenmedia', tinymce.plugins.OxygenmediaPlugin);
})();
