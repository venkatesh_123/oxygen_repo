/**
 * Provides the Oxygen Calendar class.
 *
 * @module Oxygen-form-dateselector
 */

/**
 * A class to overwrite the YUI3 Calendar in order to change the strings..
 *
 * @class M.form_Oxygencalendar
 * @constructor
 * @extends Calendar
 */
OxygenCALENDAR = function() {
    OxygenCALENDAR.superclass.constructor.apply(this, arguments);
};

Y.extend(OxygenCALENDAR, Y.Calendar, {
        initializer: function(cfg) {
            this.set("strings.very_short_weekdays", cfg.WEEKDAYS_MEDIUM);
            this.set("strings.first_weekday", cfg.firstdayofweek);
        }
    }, {
        NAME: 'Calendar',
        ATTRS: {}
    }
);

M.form_Oxygencalendar = M.form_Oxygencalendar || {};
M.form_Oxygencalendar.initializer = function(params) {
    return new OxygenCALENDAR(params);
};
