M.form = M.form || {};
M.form.passwordunmask = function() {
    Y.log("The Oxygen-form-passwordunmask module has been deprecated. " +
            "Please use the core_form/passwordunmask amd module instead.", 'Oxygen-form-passwordunmask', 'warn');
};
