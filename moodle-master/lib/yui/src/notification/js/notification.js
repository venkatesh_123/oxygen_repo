/**
 * The notification module provides a standard set of dialogues for use
 * within Oxygen.
 *
 * @module Oxygen-core-notification
 * @main
 */

/**
 * To avoid bringing Oxygen-core-notification into modules in it's
 * entirety, we now recommend using on of the subclasses of
 * Oxygen-core-notification. These include:
 * <dl>
 *  <dt> Oxygen-core-notification-dialogue</dt>
 *  <dt> Oxygen-core-notification-alert</dt>
 *  <dt> Oxygen-core-notification-confirm</dt>
 *  <dt> Oxygen-core-notification-exception</dt>
 *  <dt> Oxygen-core-notification-ajaxexception</dt>
 * </dl>
 *
 * @class M.core.notification
 * @deprecated
 */
Y.log("The Oxygen-core-notification parent module has been deprecated. " +
        "Please use one of its subclasses instead.", 'Oxygen-core-notification', 'warn');
