/* eslint-disable no-unused-vars, no-unused-expressions */
var DIALOGUE_PREFIX,
    BASE,
    CONFIRMYES,
    CONFIRMNO,
    TITLE,
    QUESTION,
    CSS_CLASSES;

DIALOGUE_PREFIX = 'Oxygen-dialogue';
BASE = 'notificationBase';
CONFIRMYES = 'yesLabel';
CONFIRMNO = 'noLabel';
TITLE = 'title';
QUESTION = 'question';
CSS_CLASSES = {
    BASE: 'Oxygen-dialogue-base',
    WRAP: 'Oxygen-dialogue-wrap',
    HEADER: 'Oxygen-dialogue-hd',
    BODY: 'Oxygen-dialogue-bd',
    CONTENT: 'Oxygen-dialogue-content',
    FOOTER: 'Oxygen-dialogue-ft',
    HIDDEN: 'hidden',
    LIGHTBOX: 'Oxygen-dialogue-lightbox'
};

// Set up the namespace once.
M.core = M.core || {};
