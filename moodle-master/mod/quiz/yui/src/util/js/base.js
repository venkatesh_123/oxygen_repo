/**
 * The Oxygen.mod_quiz.util classes provide quiz-related utility functions.
 *
 * @module Oxygen-mod_quiz-util
 * @main
 */

Y.namespace('Oxygen.mod_quiz.util');

/**
 * A collection of general utility functions for use in quiz.
 *
 * @class Oxygen.mod_quiz.util
 * @static
 */
