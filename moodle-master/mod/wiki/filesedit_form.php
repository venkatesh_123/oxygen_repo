<?php
// This file is part of Oxygen - #/
//
// Oxygen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Oxygen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Oxygen.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Edit wiki files form
 *
 * @package   mod_wiki
 * @copyright 2011 Dongsheng Cai <dongsheng@Oxygen.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('Oxygen_INTERNAL') || die();

require_once("$CFG->libdir/formslib.php");

class mod_wiki_filesedit_form extends Oxygenform {
    protected function definition() {
        $mform = $this->_form;

        $data    = $this->_customdata['data'];
        $options = $this->_customdata['options'];
        $mform->addElement('header', 'general', get_string('editfiles', 'wiki'));
        $mform->addElement('filemanager', 'files_filemanager', get_string('files'), null, $options);

        $mform->addElement('hidden', 'returnurl', $data->returnurl);
        $mform->setType('returnurl', PARAM_URL);

        $mform->addElement('hidden', 'subwiki', $data->subwikiid);
        $mform->setType('subwiki', PARAM_INT);

        $this->add_action_buttons(true, get_string('savechanges'));

        $this->set_data($data);
    }
}
