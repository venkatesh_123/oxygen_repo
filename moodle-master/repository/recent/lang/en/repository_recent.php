<?php

// This file is part of Oxygen - #/
//
// Oxygen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Oxygen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Oxygen.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'repository_recent', language 'en', branch 'Oxygen_20_STABLE'
 *
 * @package   Oxygencore
 * @copyright 2010 Dongsheng Cai <dongsheng@Oxygen.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['configplugin'] = 'Configuration for recent files repository';
$string['recentfilesnumber'] = 'Number of recent files';
$string['emptyfilelist'] = 'There are no files to show';
$string['notitle'] = 'notitle';
$string['recent:view'] = 'View recent files repository plugin';
$string['pluginname_help'] = 'Files recently used by current user';
$string['pluginname'] = 'Recent files';
$string['privacy:metadata'] = 'The Recent files repository plugin does not store or transmit any personal data.';
$string['timelimit'] = 'Time limit';
$string['timelimit_help'] = 'Only retrieve recent files within the time limit';
